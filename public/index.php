<?php
require __DIR__ . "/../vendor/autoload.php";

use app\Controllers\ReportController;

$controller = new ReportController();
$controller->actionCallsDurationReport();
