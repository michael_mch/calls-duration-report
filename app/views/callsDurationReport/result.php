<?php
require(__DIR__ . '/../layouts/header.php');

/** @var \app\Models\Reports\CallsDurationReportItem[] $reportItems */
?>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8 border">
			<h1>Calls Duration Report</h1>
			<table class="table table-striped">
				<thead>
				<tr>
					<th scope="col">CustomerId</th>
					<th scope="col">Number of calls within the same continent</th>
					<th scope="col">Total Duration of calls within the same continent</th>
					<th scope="col">Total number of all calls</th>
					<th scope="col">The total duration of all calls</th>
				</tr>
				</thead>
				<tbody>
                <?php foreach ($reportItems as $item): ?>
					<tr scope="row">
						<td><?= $item->customerId; ?></td>
						<td><?= $item->sameContinentCallsCount; ?></td>
						<td><?= $item->sameContinentCallsDuration; ?></td>
						<td><?= $item->totalCallsCount; ?></td>
						<td><?= $item->totalCallsDuration; ?></td>
					</tr>
                <?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
<?php

require(__DIR__ . '/../layouts/footer.php');
