<?php


namespace app\Models\Parsers;


use app\Models\Call;
use app\Models\Customer;

/**
 * Class CustomersCallsParser
 *
 * @package app\Models\Parsers
 */
class CustomersCallsParser
{
    private $file;

    /**
     * @param $file
     *
     * @return CustomersCallsParser
     */
    public function setFile($file): CustomersCallsParser
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return Customer[]
     * @throws \Exception
     */
    public function parse(): array
    {
        $tmpFile = $this->file['tmp_name'];
        /** @var resource $file */
        if (!is_uploaded_file($tmpFile) || ($file = fopen($tmpFile, 'r')) == false) {

            throw new \Exception('File was not uploaded');
        }

        $result = [];
        while (($data = fgetcsv($file, 200)) != false) {
            $customerId = $data[0];
            if (!isset($result[$customerId])) {
                $customer = new Customer();
                $customer->id = $customerId;
                $result[$customerId] = $customer;
            }
            $call = new Call();
            $call->customerId = $customerId;
            $call->startedAt = $data[1];
            $call->duration = $data[2];
            $call->phoneNumber = $data[3];
            $call->ip = $data[4];
            $result[$customerId]->calls[] = $call;

            if (!preg_match('/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/', $call->ip)
                || !preg_match('/\d{1,15}/', $call->phoneNumber)) {
                throw new \Exception('Invalid data in file.');
            }
        }

        return $result;
    }

}