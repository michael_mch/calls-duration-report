<?php


namespace app\Models;


/**
 * Class Customer
 *
 * @package app\Models
 */
class Customer
{
    /** @var integer */
    public $id;
    /** @var Call[] */
    public $calls = [];
}