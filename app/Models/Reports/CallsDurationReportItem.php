<?php


namespace app\Models\Reports;


/**
 * Class CallsDurationReportItem
 *
 * @package app\Models\Reports
 */
class CallsDurationReportItem
{
    /** @var integer */
    public $customerId = 0;
    /** @var integer */
    public $sameContinentCallsCount = 0;
    /** @var integer */
    public $sameContinentCallsDuration = 0;
    /** @var integer */
    public $totalCallsCount = 0;
    /** @var integer */
    public $totalCallsDuration = 0;
}