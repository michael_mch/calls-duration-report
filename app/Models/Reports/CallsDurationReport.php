<?php


namespace app\Models\Reports;


use app\Models\Customer;
use app\Models\Resources\IpLocationInterface;
use app\Models\Resources\IpLocationProxy;
use app\Models\Resources\PhoneNumberLocation;
use app\Models\Resources\PhoneNumberLocationInterface;

/**
 * Class CallsDurationReport
 *
 * @package app\Models
 */
class CallsDurationReport
{
    /** @var Customer[] */
    private $customers = [];
    /** @var IpLocationInterface */
    private $ipLocationModel;
    /** @var PhoneNumberLocationInterface */
    private $phoneNumberLocationModel;

    public function __construct()
    {
        $this->phoneNumberLocationModel = new PhoneNumberLocation();
        $this->ipLocationModel = new IpLocationProxy();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function calcReport(): array
    {
        $result = [];
        foreach ($this->customers as $customer) {
            $reportItem = new CallsDurationReportItem();
            $reportItem->customerId = $customer->id;
            foreach ($customer->calls as $call) {
                $ipContinentCode = $this->ipLocationModel->setIp($call->ip)
                    ->getLocation()
                    ->getContinentCode();
                $phoneNumberContinentCode = $this->phoneNumberLocationModel->setPhoneNumber($call->phoneNumber)
                    ->getLocation()
                    ->getContinentCode();
                if ($ipContinentCode === $phoneNumberContinentCode) {
                    $reportItem->sameContinentCallsCount ++;
                    $reportItem->sameContinentCallsDuration ++;
                }
                $reportItem->totalCallsCount ++;
                $reportItem->totalCallsDuration ++;
            }
            $result[] = $reportItem;
        }

        return $result;
    }

    /**
     * @param Customer[] $customers
     *
     * @return CallsDurationReport
     */
    public function setCustomers(array $customers): CallsDurationReport
    {
        $this->customers = $customers;
        return $this;
    }

}