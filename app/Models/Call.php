<?php


namespace app\Models;


/**
 * Class Call
 *
 * @package app\Models
 */
class Call
{
	/** @var integer */
	public $customerId;
	/** @var string */
	public $startedAt;
	/** @var string */
	public $phoneNumber;
	/** @var string */
	public $ip;
	/** @var integer */
	public $duration;
}