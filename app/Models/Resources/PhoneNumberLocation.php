<?php


namespace app\Models\Resources;


use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\ItemInterface;

class PhoneNumberLocation implements PhoneNumberLocationInterface
{
    const COUNTRIES_EXPIRES_TIME = 60 * 60 * 24;
    const FILE_PATH = __DIR__ . '/../../../storage/countryInfo.csv';
    const COLUMNS_COUNT = 19;

    /** @var string */
    private $phoneNumber;
    /** @var string */
    private $url = 'http://download.geonames.org/export/dump/countryInfo.txt';
    /** @var array  */
    private $countries = [];
    /** @var array  */
    private $columns = [];

    /**
     * PhoneNumberLocation constructor.
     */
    public function __construct()
    {
        try {
            $this->countries = Cache::getInstance()->get('countries', function (ItemInterface $item) {
                $item->expiresAfter(self::COUNTRIES_EXPIRES_TIME);

                return $this->parseCountryInfo();
            });
        } catch (InvalidArgumentException $e) {
            $this->countries = $this->parseCountryInfo();
        }
    }

    /**
     * @param mixed $phoneNumber
     *
     * @return PhoneNumberLocation
     */
    public function setPhoneNumber(string $phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return Location
     * @throws \Exception
     */
    public function getLocation(): Location
    {
        $location = new Location();

        foreach ($this->countries as $key => $continent) {
            if (strpos($this->phoneNumber, strval($key)) === 0) {
                $location->setContinentCode($continent);
                return $location;
            }
        }

        throw new \Exception('Country code not found');
    }

    /**
     * @return array
     */
    private function parseCountryInfo(): array
    {
        if ($file = fopen(self::FILE_PATH, 'w')) {
            fwrite($file, file_get_contents($this->url));
            fclose($file);
        }

        $file = fopen(self::FILE_PATH, 'r');

        $result = [];
        /** @var resource $file */
        if ($file !== false) {
            while (($data = fgetcsv($file, 1000, "\t")) !== false) {
                if (!$this->columns) {
                    $this->checkIsColumnDefinition($data);
                    continue;
                }

                $array = array_combine($this->columns, $data);
                $code = preg_replace("/[^0-9]/", "", $array['Phone']);

                $result[$code] = $array['Continent'];
            }
            fclose($file);
        }

        return $result;
    }

    private function checkIsColumnDefinition($row)
    {
        $ret = false;

        if (empty($this->columns) && count($row) == self::COLUMNS_COUNT) {
            $ret = true;
            foreach ($row as $k => $column) {
                if (!trim($column)) {
                    //if column is empty its not a definition row
                    $ret = false;
                    break;
                }
            }
        }

        if ($ret) {
            $this->columns = $row;
        }

        return $ret;
    }

}