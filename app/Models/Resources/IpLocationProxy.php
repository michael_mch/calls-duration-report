<?php


namespace app\Models\Resources;


use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\ItemInterface;

class IpLocationProxy implements IpLocationInterface
{
    const IP_EXPIRES_TIME = 60 * 60 * 24;

    /** @var IpLocationInterface */
    private $apiModel;
    /** @var string */
    private $ip;

    /**
     * @param string $ip
     *
     * @return self
     */
    public function setIp(string $ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return Location
     * @throws \Exception
     */
    public function getLocation(): Location
    {
        try {
            return Cache::getInstance()->get($this->ip, function (ItemInterface $item) {
                $item->expiresAfter(self::IP_EXPIRES_TIME);

                return $this->getLocationFromApi();
            });
        } catch (InvalidArgumentException $e) {
            return $this->getLocationFromApi();
        }
    }

    /**
     * @return IpLocationInterface
     */
    private function getApiModel(): IpLocationInterface
    {
        if (!$this->apiModel) {
            $this->apiModel = new IpStackApi();
        }

        return $this->apiModel;
    }

    /**
     * @return Location
     * @throws \Exception
     */
    private function getLocationFromApi(): Location
    {
        return $this->getApiModel()
            ->setIp($this->ip)
            ->getLocation();
    }

}