<?php


namespace app\Models\Resources;


/**
 * Interface PhoneNumberLocationInterface
 *
 * @package app\Models\Resources
 */
interface PhoneNumberLocationInterface
{
    /**
     * @param string $phoneNumber
     *
     * @return self
     */
    public function setPhoneNumber(string $phoneNumber);

    /**
     * @return Location
     * @throws \Exception
     */
    public function getLocation(): Location;
}