<?php


namespace app\Models\Resources;


class Location
{
    /** @var string */
    private $continentCode;

    /**
     * @return string
     */
    public function getContinentCode(): string
    {
        return $this->continentCode;
    }

    /**
     * @param mixed $continentCode
     *
     * @return Location
     */
    public function setContinentCode($continentCode): Location
    {
        $this->continentCode = $continentCode;

        return $this;
    }

}