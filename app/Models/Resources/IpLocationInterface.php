<?php


namespace app\Models\Resources;


/**
 * Interface IpLocationInterface
 *
 * @package app\Models\Resources
 */
interface IpLocationInterface
{
    /**
     * @param string $ip
     *
     * @return self
     */
    public function setIp(string $ip);

    /**
     * @return Location
     * @throws \Exception
     */
    public function getLocation(): Location;

}