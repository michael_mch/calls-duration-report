<?php


namespace app\Models\Resources;


/**
 * Class IpStackApi
 *
 * @package app\Models\Resources
 */
class IpStackApi implements IpLocationInterface
{
    /** @var string */
    private $apiKey = 'cf20c248c147511e68673d93e41688d1';
    /** @var string */
    private $url = 'http://api.ipstack.com/';
    /** @var string */
    private $ip;

    /**
     * @param string $ip
     *
     * @return $this|IpLocationInterface
     */
    public function setIp(string $ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return Location
     * @throws \Exception
     */
    public function getLocation(): Location
    {
        $response = $this->request();
        $location = new Location();
        $location->setContinentCode($response['continent_code']);

        return $location;
    }

    /**
     * @return string
     */
    protected function makeUrl(): string
    {
        return $this->url . $this->ip . '?access_key=' . $this->apiKey;
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function request(): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $this->makeUrl());
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if ($error) {
            throw new \Exception($error);
        }

        $response = json_decode($result, true);

        if (isset($response['success']) && $response['success'] === false) {
            $error = $response['error']['info'] ?? 'Something went wrong';

            throw new \Exception($error);
        }

        return $response;
    }
}