<?php


namespace app\Models\Resources;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * Class Cache
 *
 * @package app\Models\Resources
 */
class Cache
{
    /** @var FilesystemAdapter */
    private static $cachePool;

    /**
     * @return FilesystemAdapter
     */
    public static function getInstance()
    {
        if (!self::$cachePool) {
            self::$cachePool = new FilesystemAdapter();
        }

        return self::$cachePool;
    }
}