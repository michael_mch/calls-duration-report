<?php

namespace app\Controllers;

use app\Models\Reports\CallsDurationReport;
use app\Models\Parsers\CustomersCallsParser;

/**
 * Class ReportController
 *
 * @package app\Controllers
 */
class ReportController extends Controller
{

    public function actionCallsDurationReport()
    {
        if (isset($_FILES['file'])) {
            try {
                $parser = new CustomersCallsParser();
                $parser->setFile($_FILES['file']);
                $customers = $parser->parse();

                $report = new CallsDurationReport();
                $reportItems = $report->setCustomers($customers)
                    ->calcReport();
            } catch (\Exception $e) {
                die($e->getMessage());
            }
            include __DIR__ . '/../views/callsDurationReport/result.php';
            die;
        }

        include __DIR__ . '/../views/callsDurationReport/form.php';
    }
}